using System;
using Cloudburst.Configuration;
using Cloudburst.EventBus;
using Cloudburst.HealthChecks;
using Cloudburst.Logging;
using Cloudburst.ServiceRegistration;
using Cloudburst.Version;
using Cloudburst.Version.Abstractions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.SystemConsole.Themes;

namespace Cloudburst.Core {
	public static class Cloudburst {

		/*

		public static void Main(string[] args) => Cloudburst.Downpour<Startup, AppVersion>(args);

		 public static void Main(string[] args) =>
			Cloudburst.Downpour<Startup, AppVersion>(args, webBuilder => {
				// We configure the listening options here mainly so that there is no SSL/TLS.
				// SSL/TLS is great for the public Internet, but why waste the overhead when
				// you are behind an API Gateway? Not to mention, managing certificates for
				// a lot of microservices is a headache.
				webBuilder.ConfigureKestrel(options => {
					options.ListenAnyIP(5000, listenOptions => {
						listenOptions.Protocols = HttpProtocols.Http1;
					});

					options.ListenAnyIP(5001, listenOptions => {
						listenOptions.Protocols = HttpProtocols.Http2;
					});
				});
			});

		 */

		public static void Downpour<TStartup, TServiceVersion>(string[] args, Action<IWebHostBuilder> webBuilder = null, Action<IHostBuilder> hostBuilder = null) where TServiceVersion : class, IServiceVersion where TStartup : class {
			try {
				// Create a logger now to log any exceptions during Host Startup.
				// It will be replaced later on with a logger configured from the settings.
				Log.Logger = new LoggerConfiguration()
					.Enrich.FromLogContext()
					.MinimumLevel.Debug()
					.MinimumLevel.Override("Microsoft", LogEventLevel.Debug)
					.MinimumLevel.Override("System", LogEventLevel.Debug)
					.WriteTo.Console(theme: AnsiConsoleTheme.Code)
					.CreateLogger();

				IHostBuilder cloudburstHostBuilder = Host.CreateDefaultBuilder(args)
					.UseCloudburstVersioning<TServiceVersion>()
					.UseCloudburstConfiguration<TServiceVersion>()
					.UseCloudburstLogging()
					.UseCloudburstHealthCheck()
					.UseCloudburstServiceRegistration()
					.UseCloudburstEventBus();

				hostBuilder?.Invoke(cloudburstHostBuilder);

				IHost host = cloudburstHostBuilder
					.ConfigureWebHostDefaults(wb => {
						wb.UseStartup<TStartup>();
						webBuilder?.Invoke(wb);
					})
					.Build();

				host.Run();
			} catch (Exception exception) {
				Log.Fatal(exception, "Host TERMINATED unexpectedly.");
			} finally {
				Log.CloseAndFlush();
			}
		}
	}
}

// app.UseSerilogRequestLogging();
