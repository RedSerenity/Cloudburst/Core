using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;

namespace Cloudburst.Core {
	public static class CloudburstExtensions {
		public static IWebHostBuilder ConfigurePorts(this IWebHostBuilder webBuilder, int httpPort, int http2Port) {
			webBuilder.ConfigureKestrel(options => {
				options.ListenAnyIP(httpPort, listenOptions => {
					listenOptions.Protocols = HttpProtocols.Http1;
				});

				options.ListenAnyIP(http2Port, listenOptions => {
					listenOptions.Protocols = HttpProtocols.Http2;
				});
			});

			return webBuilder;
		}
	}
}
